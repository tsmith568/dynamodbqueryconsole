﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Amazon;
using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.DataModel;
using DynamoDBQueryConsole.Model;

namespace DynamoDBQueryConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                var Customers = Task.Run(() => GetData()).GetAwaiter().GetResult();
                Customers.ForEach(x => Console.WriteLine(x.FirstName + " " + x.LastName));

            }
            catch (Exception ex)
            {

                throw;
            }
        }


        public static async Task<List<Customer>> GetData()
        {
            var dbname = "Customer";
            var dClient = new AmazonDynamoDBClient("AKIAQBRC2XKOJMN4LMQQ", "2vm+wecU0ULkJjYpN+puTNPeJrs/E6naWQ0z8p29", RegionEndpoint.USEast1);
            var customers = new List<Customer>();

            using (var dbContext = new DynamoDBContext(dClient))
            {
                //var singeCustomer = await dbContext.LoadAsync<Customer>("397EA2FA-B7F2-4865-A22B-80DFA70EF690");  //syntax to retrieve a single log entry

                //QueryAsync provides many query options.
                //Since the primary key is a unique customer identifier, there will every only be one customer that meets this guid filter.  
                //But if the primary key was an environment or a redundant item...then you would get multiple.
                customers = await dbContext.QueryAsync<Customer>("397EA2FA-B7F2-4865-A22B-80DFA70EF690", new DynamoDBOperationConfig { OverrideTableName = dbname, IndexName = "CustomerId-index" }).GetRemainingAsync();
            }

            return customers;
        }


    }
}
